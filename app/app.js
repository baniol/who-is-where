/**
 * Copyright (c) 2014 Mateusz Gachowski, contributors.
 * Licensed under the MIT license (see LICENSE file).
 */
(function () {
  'use strict';

  var App;
  var Settings;

  Settings = {
    //apiBase : 'http://mobi-api.projectalfa.eu/api'
    apiBase : 'http://localhost:3000/api'
  };

  App = angular.module('wiwApp', ['ngDraggable', 'angular.filter']);

  var addAuth = function (data) {
    var loginData = JSON.parse(localStorage.getItem('loginData'));
    data.auth = loginData;

    return data;
  };

  App.factory('authenticateFactory', function ($http) {
    return function (data) {
      return $http({
        url: Settings.apiBase + '/authenticate',
        method: 'POST',
        data: 'login=' + data.login + '&password=' + data.password,
        headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
      });
    };
  });

  App.factory('desksFactory', function ($http) {
    return $http.get(Settings.apiBase + '/getDesks');
  });

  App.factory('peopleFactory', function ($http) {
    return $http.get(Settings.apiBase + '/getPeople');
  });

  App.factory('deviceFactory', function ($http) {
    return {
      getData: function () {
        return $http.get(Settings.apiBase + '/getDevices', {cache: false});
      }
    };
  });

  App.factory('personFactory', function ($http) {
    return {
      'get'    : function (personId) {
        return $http.get(Settings.apiBase + '/getPersonById/' + personId);
      },
      'update' : _.compose(function (personData) {
        var personId = personData.id;

        return $http({
          url: Settings.apiBase + '/updatePerson/' + personId,
          method: 'POST',
          data: personData,
          headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        });
      }, addAuth),
      'add' : _.compose(function (obj) {

        var loginData = JSON.parse(localStorage.getItem('loginData'));

        return $http({
          url: Settings.apiBase + '/addPerson',
          data : obj,
          method : 'POST',
          headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        });
      }, addAuth)
    };
  });


  window.App = App;

})();
