/**
 * Copyright (c) 2014 Mateusz Gachowski, contributors.
 * Licensed under the MIT license (see LICENSE file).
 */
(function () {
  'use strict';

  App.controller('ProfileController', ['$scope', 'personFactory', function ($scope, personFactory) {
    $scope.removeFromDesk = function () {

      $scope.desks[$scope.userProfile.deskId - 1].owner = undefined;
      $scope.userProfile.deskId = null;

      personFactory.update($scope.userProfile).success(function () {
        $('#user-profile-modal').modal('hide');
        $scope.unassignedPeople.push($scope.userProfile);
        $scope.userProfile = null;
      });
    };
  }]);
})();