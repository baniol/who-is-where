/**
 * Copyright (c) 2014 Mateusz Gachowski, contributors.
 * Licensed under the MIT license (see LICENSE file).
 */
(function () {
  'use strict';

  App.controller('LoginController', ['$scope', 'authenticateFactory', function ($scope, authenticateFactory) {
    
    $scope.sendLoginData = function (event) {
      event.preventDefault();

      var loginFormData = $(event.target).serializeArray();
      var loginModal = $('#loginModal');

      var loginData = _.zipObject(_.pluck(loginFormData, 'name'), _.pluck(loginFormData, 'value'));

      authenticateFactory(loginData).success(function (data) {
        $scope.$parent.$parent.loggedIn = true;
        loginModal.modal('hide');
        localStorage.setItem('loginData', JSON.stringify(loginData));
      }).error(function (data) {
        $('.form-group', loginModal).addClass('has-error');
      });
    };
  }]);
})();