/**
 * Copyright (c) 2014 Mateusz Gachowski, contributors.
 * Licensed under the MIT license (see LICENSE file).
 */
(function () {
  'use strict';

  App.controller('OfficeController', ['$scope', 'desksFactory', 'deviceFactory', 'peopleFactory', 'personFactory', '$filter', function ($scope, desksFactory, deviceFactory,  peopleFactory, personFactory, $filter) {

    var orderBy = $filter('orderBy');

    $scope.loggedIn         = (function () {
      return !!(JSON.parse(localStorage.getItem('loginData')));
    })();
    $scope.desksInverted    = false;
    $scope.showNumbers      = false;
    $scope.showTeams        = false;
    $scope.showProjects     = false;
    $scope.showDashboard    = false;
    $scope.showDeviceList      = false;

    $scope.userProfile      = null;
    $scope.profileVisible   = false;

    $scope.unassignedPeople = [];

    desksFactory.success(function (desks) {
      var data = desks.data;
      data = _.map(data, function (desk) {
        desk.x = ~~desk.x;
        desk.y = ~~desk.y;
        return desk;
      });
      $scope.desks = data;
    });

    $scope.loadDevices = function () {
      deviceFactory.getData().success(function (devices) {
        $scope.devices = devices;
        //$scope.order('deviceName', false);
      });
    };

    $scope.order = function(predicate, reverse) {
      $scope.devices = orderBy($scope.devices, predicate, reverse);
      console.log($scope.devices);
    };

    $scope.loadDevices();

    peopleFactory.success(function (people) {
      $scope.peopleList = people.data;
      _.each(people.data, function (person) {
        if (person.deskId) {
          $scope.desks[person.deskId - 1].owner = person;
        } else {
          $scope.unassignedPeople.push(person);
        }
      });
    });

    $scope.toggleNumbers = function ($event) {
      $event.preventDefault();
      $scope.showNumbers = !$scope.showNumbers;
    };

    $scope.rotateNames = function ($event) {
      $event.preventDefault();

      _.each($scope.desks, function (desk) {
        if ($scope.desksInverted) {
          desk.x = desk.x + 10;
          desk.y = desk.y - 8;
        }
        else {
          desk.x = desk.x - 10;
          desk.y = desk.y + 8;
        }
      });
      $scope.desksInverted = !$scope.desksInverted;
    };

    $scope.toggleTeams = function ($event) {
      $event.preventDefault();

      $scope.showTeams = !$scope.showTeams;

      if ($scope.showProjects) {
        $scope.showProjects = false;
      }
    };

    $scope.toggleProjects = function ($event) {
      $event.preventDefault();

      $scope.showProjects = !$scope.showProjects;

      if ($scope.showTeams) {
        $scope.showTeams = false;
      }
    };

    $scope.toggleDevices = function ($event) {
      $event.preventDefault();
      $scope.showDevices = !$scope.showDevices;
    };

    $scope.toggleDeviceList = function ($event) {
      $event.preventDefault();
      $scope.showDeviceList = !$scope.showDeviceList;
    };

    $scope.peopleCount = function () {
      return _.reduce($scope.desks, function (sum, desk, key) {
        if (desk.owner) {
          return sum + 1;
        }
        return sum;
      }, 0);
    };

    $scope.editPeople = function ($event) {
      $event.preventDefault();

      $scope.showDashboard = !$scope.showDashboard;
    };

    $scope.onDropComplete = function (index) {

      var drop;
      var callback;
      var assign;

      drop = this;

      callback = function () {
        $scope.unassignedPeople.push(drop.desk.owner);
        assign();
      };

      assign = function () {
        var deskId = drop.desk.id;

        $scope.unassignedPeople[index].deskId = deskId;

        personFactory.update($scope.unassignedPeople[index]).success(function () {
          $scope.desks[deskId - 1].owner = $scope.unassignedPeople[index];
          $scope.unassignedPeople.splice(index, 1);
        });
      };

      if (drop.desk.owner) {
        drop.desk.owner.deskId = null;
        personFactory.update(drop.desk.owner).success(callback);
      }
      else {
        assign();
      }

    };

    $scope.addPerson = function ($event) {
      $event.preventDefault();

      var name = $('form input:not([type=button])').val();
      var obj = {
          name : name,
          position : "",
          birthday : null,
          deskId : null,
          teamId : null,
          projectId: null,
          image : ""
        };

      if (name) {
        personFactory.add(obj).success(function (data) {
          obj.id = data.id;
          $scope.unassignedPeople.push(obj);
        });

      }

      $('form input:not([type=button])').val('');
    };

    $scope.logout = function () {
      localStorage.removeItem("loginData");
      $scope.loggedIn = false;
      $scope.showDashboard = false;
    };
  }]);
})();
